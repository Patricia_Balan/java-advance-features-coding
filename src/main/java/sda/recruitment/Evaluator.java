package sda.recruitment;

import sda.candidate.Candidate;
import sda.candidate.CandidateStatus;

public interface Evaluator {


    void evaluate(Candidate candidate);

}
