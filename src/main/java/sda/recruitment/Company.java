package sda.recruitment;

import sda.candidate.Candidate;
import sda.department.DepartmentName;
import sda.department.Marketing;
import sda.department.Production;
import sda.exceptions.EvaluationIncapacityException;

import java.util.ArrayList;
import java.util.List;

public class Company {

    private final String name;
    private final List<Candidate> candidates;

    // pt initializare lista trebuie creat un constructor

    public Company(String name, List<Candidate> candidates) {
        this.name = name;
        this.candidates = candidates;
    }


    public void recruiting() {

        Marketing marketingDep = new Marketing();
        Production productionDep = new Production();

        for (Candidate c: candidates) {
            if(c.getDepartmentName().equals(DepartmentName.PRODUCTION)) {
                productionDep.evaluate(c);
            } else if (c.getDepartmentName().equals(DepartmentName.MARKETING)) {
                marketingDep.evaluate(c);
            } else {
                throw new EvaluationIncapacityException();

                // nu da exceptie la throw si compileaza ca in evaluationincap[acity exception
                // am extins RuntimeException si nu exception.
                // daca extindeam exception trebuia la recruiting sa completam cu trows EvaluationIncapacityException.
            }
        }
    }
}
