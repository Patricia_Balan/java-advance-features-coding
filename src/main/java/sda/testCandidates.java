package sda;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import sda.candidate.Candidate;
import sda.department.Department;
import sda.department.DepartmentName;
import sda.department.Marketing;
import sda.department.Production;
import sda.recruitment.Company;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class testCandidates {

    public static void main(String[] args) throws IOException {


        // nu mai e necesara pt ca citim din fisiserului initial
//        Candidate candidate1 = new Candidate();
//        Candidate candidate2 = new Candidate("Pop Ion", 30, "Address", "pi@yahoo", 5, 2, DepartmentName.MARKETING);
//        Candidate candidate3 = new Candidate("Pop Maria", 25, "Address", "pi@yahoo", 7, 10, DepartmentName.MARKETING);
//        Candidate candidate4 = new Candidate("Pop Marius", 33, "Address", "pi@yahoo", 3, 2, DepartmentName.PRODUCTION);
//        Candidate candidate5 = new Candidate("Pop Ion", 28, "Address", "pi@yahoo", 7, 2, DepartmentName.PRODUCTION);
//
//        List<Candidate> candidates = new ArrayList<>();
//        candidates.add(candidate2);
//        candidates.add(candidate3);
//        candidates.add(candidate4);
//        candidates.add(candidate5);


        // aici am citit fisierul si se leaga de stergerea .. new candidate.
        List<Candidate> candidates = readCandidatesToJason();

        Company company = new Company("SDA Recruiting", candidates);
        company.recruiting();

        System.out.println(candidates);

        writeCandidatesToTxtFile(candidates);
        writeCandidatesToJason(candidates);
    }

    public static void writeCandidatesToTxtFile(List<Candidate> candidates) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("candidates-accepted.txt"))) {
            writer.write(candidates.toString());

        } catch (IOException e) {
            System.out.println("Could not write to file!");
        }
    }

        // am folosit try catch dar puteam folosi si throws IOException...
//        FileWriter fileWriter = new FileWriter("candidates-accepted.txt");
//        BufferedWriter writer;
//        writer = new BufferedWriter(fileWriter);
//
//        try {
//            writer.write(candidates.toString());
//            writer.close();
//        } catch (IOException e) {
//            System.out.println("Could not write to file!");
//        } finally {
//            try {
//                writer.close();
//            } catch (IOException e) {
//            }
//        }

    public static void writeCandidatesToJason(List<Candidate> candidates) throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new File("candidati-acceptati.json"), candidates);
    }

    public static List<Candidate> readCandidatesToJason() throws IOException{

        // pt returnarea valorilor citite am pus inainte de objectMapper List<> candidates si automat un return,
        // iar metoda nu a mai fost void si am adaugat List<Candidate>

        ObjectMapper objectMapper = new ObjectMapper();
        List<Candidate> candidates =
                objectMapper.readValue(new File("candidati-initiali.json"),
                        new TypeReference<List<Candidate>>(){});
        return candidates;
    }

    // nu mai sunt necesare - au fost pt testarea metodelor avuta in marketin/production
//        Marketing marketingDep = new Marketing();
//        marketingDep.evaluate(candidate2);
//        marketingDep.evaluate(candidate3);

//        System.out.println(candidate2);
//        System.out.println(candidate3);
//
//        Production productionDep = new Production();
//        productionDep.evaluate(candidate4);
//        productionDep.evaluate(candidate5);
//
//        System.out.println(candidate4);
//        System.out.println(candidate5);
}
