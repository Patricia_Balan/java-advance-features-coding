package sda.department;

import sda.candidate.CandidateStatus;
import sda.recruitment.Evaluator;

public abstract class Department implements Evaluator {

    // PROTECTED - vizibile atat in pachet cat si in clasele care mostenesc

    protected DepartmentName name;
    protected int minLevelOfCompetence;


}
