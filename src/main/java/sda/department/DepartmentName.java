package sda.department;

public enum DepartmentName {

    MARKETING,
    PRODUCTION,
    HR,
    IT,
    ACCOUNTING
}
