package sda.candidate;

import sda.department.DepartmentName;

public class Candidate {

    private String name;
    private Integer age;
    private String address;
    private String emailAddress;
    private int levelOfCompetence;
    private int yearsOfExperience;
    private DepartmentName departmentName;
    private CandidateStatus candidateStatus;

    public Candidate(String name,
                     Integer age,
                     String addresss,
                     String emailAddress,
                     int levelOfCompetence,
                     int yearsOfExperience,
                     DepartmentName departmentName) {

        // statusul nu in constructor, ca daca e acolo va putea fi modificat de oricine oricum
        // se initializeaza conform cerintei cu "awaiting"
        this.name =  name;
        this.age = age;
        this.address = addresss;
        this.emailAddress = emailAddress;
        this.levelOfCompetence = levelOfCompetence;
        this.yearsOfExperience = yearsOfExperience;
        this.departmentName = departmentName;
        this.candidateStatus = CandidateStatus.AWAITING;
    }

    public Candidate () {
    }

    public String getName() {
        return this.name;
    }

    public void setName(String numeleCandidatului) {
        this.name = numeleCandidatului;
    }

    public CandidateStatus getCandidateStatus() {
        return candidateStatus;
    }

    public Integer getAge () {
        return age;
    }

    public void setAge (Integer age) {
        this.age = age;
    }

    public String getAddress () {
        return this.address;
    }

    public void setAddress (String address) {
        this.address = address;
    }

    public String getEmailAddress () {
        return this.emailAddress;
    }

    public void setEmailAddress (String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getLevelOfCompetence () {
        return this.levelOfCompetence;
    }

    public void setLevelOfCompetence (int levelOfCompetence) {
        this.levelOfCompetence = levelOfCompetence;
    }

    public int getYearsOfExperience() {
        return this.yearsOfExperience;
    }

    public void setYearsOfExperience (int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public void setCandidateStatus (CandidateStatus candidateStatus) {
        this.candidateStatus = candidateStatus;
    }

    public DepartmentName getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(DepartmentName departmentName) {
        this.departmentName = departmentName;
    }

    public String toString() {
        return "Candidate:" + name + " has " + yearsOfExperience +
                " years of competence, level of competence " + levelOfCompetence +
                ", applied for " + departmentName +
                " and has status " + candidateStatus + "\n";
    }

//    @Override
//    public String toString() {
//        return "Candidate{" +
//                "name='" + name + '\'' +
//                ", age=" + age +
//                ", address='" + address + '\'' +
//                ", emailAddress='" + emailAddress + '\'' +
//                ", levelOfCompetence=" + levelOfCompetence +
//                ", yearsOfExperience=" + yearsOfExperience +
//                ", departmentName=" + departmentName +
//                ", candidateStatus=" + candidateStatus +
//                '}';
//    }
}
