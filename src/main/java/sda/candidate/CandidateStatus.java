package sda.candidate;

public enum CandidateStatus {

    ACCEPTED,
    REJECTED,
    AWAITING

}
